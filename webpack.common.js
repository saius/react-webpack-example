const path = require("path");

const {CleanWebpackPlugin} = require("clean-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const ESLintPlugin = require("eslint-webpack-plugin");
const Dotenv = require("dotenv-webpack");

console.log("Cargando configuración general de webpack");

module.exports = {
  entry: "./src/index.js",
  output: {
    path: path.resolve(__dirname, "dist"),
    publicPath: ""
  },
  plugins: [
    // limpiar dist/ en cada rebuildeo
    new CleanWebpackPlugin(),
    // generar el dist/index.html dinámicamente
    // (con este hay que tener cuidado de no settear ningún loader de html
    // en module.rules que pueda interferir)
    new HtmlWebpackPlugin({
      template: "src/index.html"
    }),
    new ESLintPlugin(),
    new Dotenv()
  ],
  module: {
    rules: [
      {
        // para hacer el código más compatible con navegadores (babel)
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: "babel-loader"
        }
      }
      // las rules de css no están acá porq son distintas en dev y prod
    ]
  },
  // para mejorar el cacheo
  // https://webpack.js.org/guides/caching/
  optimization: {
    // generar js aparte para el runtime
    runtimeChunk: "single",
    splitChunks: {
      cacheGroups: {
        // generar js aparte llamado vendors para los node_modules, todos juntos
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: "vendors",
          chunks: "all"
        }
      }
    }
  },
  resolve: {
    alias: {
      // aliases para no tener que hacer imports '../../relati.vos'
      // https://webpack.js.org/configuration/resolve/
      // para js usar https://www.npmjs.com/package/babel-plugin-module-resolver
      assets: path.resolve(__dirname, "src/assets/"),
      pages: path.resolve(__dirname, "src/pages/"),
      components: path.resolve(__dirname, "src/components/"),
      styles: path.resolve(__dirname, "src/styles/")
    }
  }
};
