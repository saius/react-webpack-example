const {merge} = require("webpack-merge");
const common = require("./webpack.common.js");

const ReactRefreshWebpackPlugin = require("@pmmmwh/react-refresh-webpack-plugin");

console.log("Cargando configuración de desarrollo de webpack");

module.exports = merge(common, {
  // opciones que setea el modo development: https://webpack.js.org/configuration/mode/#mode-development
  mode: "development",
  output: {
    filename: "[name].js"
  },
  devtool: "inline-source-map",
  // configuración de webpack-dev-server
  devServer: {
    contentBase: "./dist",
    // hot reload habilitado - https://webpack.js.org/guides/hot-module-replacement/
    hot: true
  },
  plugins: [new ReactRefreshWebpackPlugin()],
  module: {
    rules: [
      {
        test: /\.(s[ac]|c)ss$/i,
        use: ["style-loader", "css-loader", "sass-loader"]
      }
    ]
  }
});
