import React from 'react';
import ReactDOM from 'react-dom';
import {AuthProvider} from 'components/AuthContext'
import App from 'components/App';
//import 'styles/un.css';
//import 'styles/un.scss';

ReactDOM.render(
  <React.StrictMode>
    <AuthProvider>
      <App />
    </AuthProvider>
  </React.StrictMode>,
 document.getElementById('root')
);

if (module.hot)
  module.hot.accept()
