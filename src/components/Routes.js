import React from "react";
import {Switch, Route} from "react-router-dom";
import Home from "pages/Home";
import Topics from "pages/Topics";
import Users from "pages/Users";
import NotFound from "pages/NotFound";

const Routes = () => {
  return (
    <Switch>
      <Route exact path="/" component={Home} />
      <Route path="/topics" component={Topics} />
      <Route path="/users" component={Users} />
      <Route component={NotFound} />
    </Switch>
  );
};

export default Routes;
