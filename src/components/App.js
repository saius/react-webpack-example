import React from "react";
import {HashRouter} from "react-router-dom";
import Navbar from "./Navbar";
import Routes from "./Routes";

const App = () => {
  return (
    <HashRouter>
      <div>
        <Navbar />
        <Routes />
      </div>
    </HashRouter>
  );
};

export default App;
