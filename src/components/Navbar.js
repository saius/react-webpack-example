import React from "react";
import { Link } from "react-router-dom";
import styled from 'styled-components'

const Li = styled.li`
  color: blue;
  a {
    color: green;
  }
`

export default function Navbar() {
  return (
    <ul>
      <Li>
        <Link to="/">Home</Link>
      </Li>
      <Li>
        <Link to="/topics">Topics</Link>
      </Li>
      <Li>
        <Link to="/users">Users</Link>
      </Li>
    </ul>
  )
}
