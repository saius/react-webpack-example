import React, {useContext} from "react";
import {Switch, Route, Link, useRouteMatch, useParams} from "react-router-dom";
import {AuthContext} from 'components/AuthContext'

const Users = function() {
  let match = useRouteMatch();
  let authContext = useContext(AuthContext);

  return (
    <div>
      <h2>Users</h2>
      <p>{authContext.loggedIn === null ? 'CARGANDO' : (authContext.loggedIn ? 'IN' : 'OUT')}</p>
      <button onClick={() => authContext.login('usu', '123')}>LOGIN</button>
      <button onClick={authContext.logout}>LOGOUT</button>
    </div>
  );
}
export default Users;
