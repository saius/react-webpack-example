Este proyecto es para probar la combinación [Webpack](https://webpack.js.org/) + [React](https://reactjs.org/)
configurando todo a mano.

Es una continuación de [saius/webpack-basic-boilerplate](https://gitlab.com/saius/webpack-basic-boilerplate) versión/tag __0.1__. En ese repo hay documentación sobre muchas configuraciones usadas acá.

El uso de React en esta aplicación es solo para ver que funciona y que hace hot reload bien, no pretende
usar las mejores prácticas ni los métodos más nuevos de implementación.

### Librerías nuevas usadas
- `react` y `react-dom` son el core de React
- [babel/preset-react](https://babeljs.io/docs/en/babel-preset-react) para que Babel compile JSX
- [sass-loader](https://webpack.js.org/loaders/sass-loader/) para compilar archivos scss/sass
- `node-sass` como librería de compilación de scss/sass que usa el loader anterior
- `react-refresh` y [react-refresh-webpack-plugin](https://github.com/pmmmwh/react-refresh-webpack-plugin) para recargar los componentes en tiempo real en desarrollo
- [react-router-dom](https://reactrouter.com/web) para hacer que la web sea una SPA (single page application), y usar `HashRouter` para que pueda route-ear sin server/backend (sino habría que [settear bien](https://burnedikt.com/webpack-dev-server-and-routing/) el dev-server).
- [styled-components](https://styled-components.com/) para soportar CSS-in-JS de la forma de `styled.div`, etc. (su uso es recomendado: [link 1](https://medium.com/building-crowdriff/styled-components-to-use-or-not-to-use-a6bb4a7ffc21), [link 2](https://getstream.io/blog/styled-components-vs-css-stylesheets/))
- [dotenv-webpack](https://www.npmjs.com/package/dotenv-webpack) para cargar `.env` y hacer los reemplazos de `process.env` en el código
- _Todas las restantes librerías usadas se explican en [saius/webpack-basic-boilerplate](https://gitlab.com/saius/webpack-basic-boilerplate)_

### Creación del proyecto
```
npm init -y
npm i -D \
webpack@5 webpack-cli@4 webpack-merge webpack-dev-server \
html-webpack-plugin clean-webpack-plugin \
mini-css-extract-plugin css-minimizer-webpack-plugin css-loader style-loader sass-loader node-sass \
babel-loader @babel/core@7 @babel/preset-env@7 @babel/plugin-transform-runtime@7 \
react-hot-loader @hot-loader/react-dom@17 react-router-dom @babel/preset-react@7 \
styled-components \
eslint @babel/eslint-parser eslint-plugin-react eslint-webpack-plugin \
dotenv-webpack
npm i \
core-js@3.8 @babel/runtime-corejs3@7 \
react@17 react-dom@17
```

y reemplazar en `package.json` la llave `"main": ...` por ``"private": "true"``

### Buildeo/hosteo del proyecto

Para compilar con webpack:

`npx webpack --config webpack.prod.js`

y mirar los archivos compilados en `dist/` y abrir en el navegador `dist/index.html`

Para servir servidor local con auto-recarga de cambios:

`npx webpack serve --config webpack.dev.js`

y navegar a [localhost:8080](localhost:8080)

Estos comandos se pueden agregar como scripts de `npm` agregándolos en `package.json` bajo la llave `"scripts"`:
```
"build": "webpack --config webpack.prod.js",
"serve": "webpack serve --config webpack.dev.js"
```
Posteriormente se pueden correr haciendo `npm run build` o `npm run serve`.

### ESLint

Para integrar [ESLint](https://eslint.org) primero instalarlo:

`npm i -D eslint`

y después inicializarlo seleccionando las opciones pertinentes:

`npx eslint --init`

Además agregar el parser de Babel [@babel/eslint-parser](https://www.npmjs.com/package/@babel/eslint-parser) y el plugin de webpack [eslint-webpack-plugin](eslint-webpack-plugin):

`npm i -D @babel/eslint-parser eslint-webpack-plugin`

y configurar `.eslintrc.js` a gusto

Finalmente [integrarlo con tu editor de código](https://eslint.org/docs/user-guide/integrations).

### Errores comunes
#### La consola del navegador muestra códigos raros
Si la consola muestra cosas tipo "�[0m �[90m" o "▯[0m ▯[90m" hay que mirar los logs del buildeo directo desde la terminal donde se está ejecutando. Ahí se van a ver bien formateados. Esto es un error en el reportador de errores de babel-loader.
