const {
  merge
} = require('webpack-merge');
const common = require('./webpack.common.js');

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');

console.log('Cargando configuración de producción de webpack')

module.exports = merge(common, {
  // opciones que setea el modo producción: https://webpack.js.org/configuration/mode/#mode-production
  mode: 'production',
  output: {
    filename: '[name].[contenthash:8].js',
  },
  // le ponemos source maps básicos para poder debuggear en producción
  devtool: 'source-map',
  plugins: [
    // hacer que los css salgan efectivamente como archivos .css y no como
    // js dinámico dentro del js principal (comportamiento por defecto)
    new MiniCssExtractPlugin({
      filename: '[name].[contenthash:8].css',
    })
  ],
  module: {
    rules: [{
      test: /\.(s[ac]|c)ss$/i,
      use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
    }],
  },
  optimization: {
    // minimizar js
    minimize: true,
    minimizer: [
      `...`,
      // minimizar css
      new CssMinimizerPlugin(),
    ],
  },
});